import client as api_client
import collections
import config
import csv
import data
import json
import langdetect
import matplotlib.pyplot as plt
import numpy
import os
import tqdm
import youtube 

COUNTRY_CHANNELS = {
    'global': [
        'fifatv'
    ],
    'france': [
        'beinsportofficiel',
        'ftvsport',
        {'title': 'LCI', 'id': 'UCewhc0fvja891XkpIPGRMxQ'},
        {'title': 'Top Football', 'id': 'UC4iq2jwgACoAiQ6nvk644ZA'},
        {'title': 'BFMTV International', 'id': 'UCFjc3EWFnaV7fGZzlQgiDUA'}
    ],
    'croatia': [
        'sportklub',
        'HRTnovimediji'
    ]
}

ALL_LANGS = ('af,ar,bg,bn,ca,cs,cy,da,de,el,en,es,et,fa,fi,fr,gu,he,hi,hr,hu,'
    + 'id,it,ja,kn,ko,lt,lv,mk,ml,mr,ne,nl,no,pa,pl,pt,ro,ru,sk,sl,so,sq,sv,'
    + 'sw,ta,te,th,tl,tr,uk,ur,vi,zh-cn,zh-tw').split(',')

def collect_videos(client, title, channel_id=None):
    data_id = '%s_videos.jsonlines' % title
    if data.is_data(data_id):
        return data.load_jsonlines(data_id)
    else:
        if channel_id is None:
            channel = youtube.get_channel_with_custom_url(client, title)
            channel_id = channel['id']['channelId']
        videos = list(youtube.get_videos_iter(client, channel_id=channel_id,
                                            part='snippet', begin_published_at='2018-06-13T00:00:00.000Z'))
        data.save_jsonlines(data_id, videos)
        return videos

def collect_video_statistics(client, title, videos):
    data_id = '%s_video_statistics.jsonlines' % title
    if data.is_data(data_id):
        return data.load_jsonlines(data_id)
    else:
        video_ids = []
        for video in videos:
            video_ids.append(video['id']['videoId'])
        video_statistics = youtube.get_videos_by_ids(client, video_ids, part='statistics')
        data.save_jsonlines(data_id, video_statistics)
        return video_statistics

def collect_video_comments(client, video):
    video_id = video['id']['videoId']
    data_id = '%s_video_comments.json' % video_id
    if data.is_data(data_id):
        return data.load_json(data_id)
    else:
        threads = list(youtube.get_comment_threads_iter(client, video_id))
        comment_dict = {}
        for thread in threads:
            if 'totalReplyCount' in thread and thread['totalReplyCount'] > 0:
                thread_comments = list(youtube.get_comments_iter(client, thread['id']))
                comment_dict[thread['id']] = thread_comments
        threads_dict = {
            'threads': threads,
            'comments': comment_dict
        }
        data.save_json(data_id, threads_dict)
        return threads_dict

def collect_videos_comments(client, title, videos):
    print ('Collecting comments from %d videos of %s.' % (len(videos), title))
    for video in tqdm.tqdm(videos):
        collect_video_comments(client, video)

def counter_to_percents(counter):
    count_sum = sum(counter.values())
    return ', '.join(['%s: %.1f%%' % (key, (float(value) * 100 / count_sum)) for key, value in counter.most_common()])

def detect_video_comment_languages(video_id):
    data_id = '%s_video_top_comments_languages.json' % video_id
    if data.is_data(data_id):
        return data.load_json(data_id)
    else:
        comment_data_id = '%s_video_comments.json' % video_id
        if not data.is_data(comment_data_id):
            return
        threads_dict = data.load_json(comment_data_id)
        lang_dict = {}
        for thread in threads_dict['threads']:
            top_comment = thread['snippet']['topLevelComment']
            top_comment_text = top_comment['snippet']['textDisplay']
            try:
                lang = langdetect.detect(top_comment_text)
            except langdetect.lang_detect_exception.LangDetectException as e:
                if 'No features in text' in e.__repr__():
                    lang = 'UNK'
                else:
                    raise e
            lang_dict[top_comment['id']] = lang
        if threads_dict['comments']:
            print(threads_dict['comments'])
            input()
        data.save_json(data_id, lang_dict)
        return lang_dict
                
def collect_youtube_channel(client, title, channel_id=None, country=None):
    print('Collecting videos from %s.' % title)
    videos = collect_videos(client, title, channel_id=channel_id)
    video_statistics = collect_video_statistics(client, title, videos)
    print('%d videos collected.' % len(videos))
    comment_counts = [int(s['statistics']['commentCount']) if 'commentCount' in s['statistics'] else 0
                      for s in video_statistics]
    print('Total %d comments found. %d±%.2f per video.' % (
        sum(comment_counts), numpy.mean(comment_counts), numpy.std(comment_counts)))
    collect_videos_comments(client, title, videos)
    print('Detecting languages from the collected videos.')
    video_lang_dict = {}
    for video in tqdm.tqdm(videos):
        video_id = video['id']['videoId']
        lang_dict = detect_video_comment_languages(video_id)
        video_lang_dict[video_id] = lang_dict
        plot_language_distribution(title, video['snippet']['title'], lang_dict, country=country)
    return videos, video_statistics, video_lang_dict

def collect_country_channels():
    f = open('%s/collected_video_statistics.csv' % config.OUT_PATH, 'w')
    writer = csv.writer(f, delimiter=',', quotechar='"')
    writer.writerow([
        'Country',
        'Channel Title',
        'Video Title',
        'View Count',
        'Comment Count',
    ] + ALL_LANGS + ['unk'])
    country_filter = []
    client = api_client.get_client_with_key()
    for country, channels in COUNTRY_CHANNELS.items():
        if country_filter and country not in country_filter:
            continue
        print('Collect channels for %s.' % country)
        for channel in channels:
            if type(channel) == dict and 'id' in channel:
                videos, video_statistics, video_lang_dict = collect_youtube_channel(
                    client, channel['title'], channel_id=channel['id'])
                title = channel['title']
            else:
                videos, video_statistics, video_lang_dict = collect_youtube_channel(client, channel, country=country)
                title = channel
            # Write CSV rows.
            video_statistics_dict = {video['id']: video for video in video_statistics}
            for video in videos:
                video_id = video['id']['videoId']
                try:
                    video_stat = video_statistics_dict[video_id]
                except KeyError:
                    video_stat = {}
                lang_counter = collections.Counter(video_lang_dict[video_id].values())
                try:
                    view_count = video_stat['statistics']['viewCount']
                except KeyError:
                    view_count = 0
                try:
                    comment_count = video_stat['statistics']['commentCount']
                except KeyError:
                    comment_count = 0
                row = [
                    country,
                    title,
                    video['snippet']['title'],
                    view_count,
                    comment_count
                ]
                lang_count_sum = sum(lang_counter.values())
                for lang in ALL_LANGS:
                    row.append((lang_counter[lang] * 100) / lang_count_sum if lang_count_sum > 0 else 0)
                row.append(lang_counter['UNK'])
                writer.writerow(row)
    f.close()

def init_out_path():
    if not os.path.exists(config.OUT_PATH):
        os.makedirs(config.OUT_PATH)

def plot_language_distribution(channel_title, title, lang_dict, country=None):
    def filesafe(filename):
        return filename.replace('/', '-').replace(' ', '_').lower()
    filename = '%s/%s_%s_%s.png' % (config.OUT_PATH,
                                    country, channel_title, filesafe(title))
    if os.path.exists(filename):
        return
    plt.clf()
    lang_counter = collections.Counter(lang_dict.values())
    lang_counts = [(count, lang) for lang, count in lang_counter.items()]
    lang_counts.sort(reverse=True)
    labels = [lang for count, lang in lang_counts]
    sizes = [count for count, lang in lang_counts]
    # colors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral']
    # patches, texts, percents = plt.pie(sizes, colors=colors, shadow=True, startangle=90)
    patches, texts, percents = plt.pie(sizes, autopct='%1.1f%%', startangle=90)
    # patches, texts
    # print(a)
    # input()
    plt.legend(patches, labels, loc="best")
    plt.axis('equal')
    # plt.title(channel_title, loc='left', fontsize=10)
    # plt.title('%s (%d)' % (title, len(lang_dict)), fontsize=10)
    # if country is not None:
    #     plt.title(country, loc='right', fontsize=10)
    plt.title('%s (%d)\n%s (%s)' % (title, len(lang_dict), channel_title, country), fontsize=10)
    # plt.tight_layout()
    plt.savefig()

def main():
    init_out_path()
    collect_country_channels()

if __name__ == '__main__':
    main()

