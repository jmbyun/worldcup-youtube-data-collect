import apiclient as googleapiclient

def search_list(client, **params):
    return client.search().list(**params).execute();

def get_channel_with_custom_url(client, url):
    channels = client.search().list(
        q=url,
        type='channel',
        part='snippet',
        order='relevance',
        maxResults=1
    ).execute()
    if 'items' in channels and channels['items']:
        return channels['items'][0]
    else:
        return None

def get_search_iter(client, part='snippet', get_items=None, verify=None, max_count=None, **params):
    page_token = None
    count = 0
    while True:
        if page_token is None:
            search_result = search_list(
                client, part=part, maxResults=10, **params)
        else:
            search_result = search_list(client, part=part, pageToken=page_token, **params)
        done = False
        if get_items is not None:
            items = get_items(search_result)
        else:
            items = search_result['items']
        done = False
        # print('==ITEMS==')
        # print(items)
        for item in items:
            if verify is not None and not verify(item):
                # print('verify fail done')
                done = True
                break
            yield item
            count += 1
            if max_count is not None and count >= max_count:
                done = True
                break
        if done or ('nextPageToken' not in search_result):
            # if 'nextPageToken' not in search_result:
            #     print('no next page done')
            break
        page_token = search_result['nextPageToken']

def get_videos_iter(client, channel_id, part='snippet', begin_published_at=None, max_count=None):
    if begin_published_at is None:
        verify = None
    else:
        verify = lambda x: x['snippet']['publishedAt'] >= begin_published_at
    return get_search_iter(client, part=part, channelId=channel_id, order='date', type='video', verify=verify, max_count=max_count)

def get_videos_by_ids(client, video_ids, part='statistics'):
    items = []
    while video_ids:
        curr_video_ids = []
        for i in range(10):
            if video_ids:
                curr_video_ids.append(video_ids.pop())
        results = client.videos().list(id=','.join(curr_video_ids), part='statistics').execute()
        for item in results['items']:
            items.append(item)
    return items
    
def get_comment_threads_iter(client, video_id, part='snippet', max_count=None):
    page_token = None
    count = 0
    error_count = 0
    while True:
        try:
            if page_token is None:
                results = client.commentThreads().list(
                    part=part, videoId=video_id, textFormat='plainText').execute()
            else:
                results = client.commentThreads().list(part=part, videoId=video_id,
                                                    textFormat='plainText', pageToken=page_token).execute()
            error_count = 0
        except googleapiclient.errors.HttpError as e:
            if 'disabled comments' in e.__repr__():
                return []
            elif 'parameter could not be found' in e.__repr__():
                return []
            elif 'this can be a transient error' in e.__repr__():
                error_count += 1
                if error_count < 5:
                    continue
                else:
                    raise e
            else:
                raise e
        done = False
        for item in results['items']:
            yield item
            count += 1
            if max_count is not None and count >= max_count:
                done = True
                break
        if done or ('nextPageToken' not in results):
            break
        page_token = results['nextPageToken']

def get_comments_iter(client, thread_id, part='snippet', max_count=None):
    page_token = None
    count = 0
    while True:
        if page_token is None:
            results = client.comments().list(
                part=part, parentId=thread_id, textFormat='plainText').execute()
        else:
            results = client.comments().list(part=part, parentId=thread_id,
                                             pageToken=page_token).execute()
        done = False
        for item in results['items']:
            yield item
            count += 1
            if max_count is not None and count >= max_count:
                done = True
                break
        if done or ('nextPageToken' not in results):
            break
        page_token = results['nextPageToken']
