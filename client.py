from apiclient.discovery import build
import constants
import config

def get_client_with_key():
    client = build(
        constants.API_SERVICE_NAME, 
        constants.API_VERSION, 
        developerKey=config.API_KEY
    )
    return client
