import os
import json
import config

def get_data_root_path():
    try:
        return config.DATA_PATH
    except:
        return './data'

def get_data_path(data_id):
    data_root_path = get_data_root_path()
    return os.path.join(data_root_path, data_id)

def init_data_root_path():
    data_root_path = get_data_root_path()
    if not os.path.exists(data_root_path):
        os.makedirs(data_root_path)

def is_data(data_id):
    path = get_data_path(data_id)
    return os.path.exists(path)

def save_json(data_id, data):
    path = get_data_path(data_id)
    f = open(path, 'w')
    f.write(json.dumps(data))
    f.close()

def load_json(data_id):
    path = get_data_path(data_id)
    f = open(path, 'r')
    data = json.loads(f.read())
    f.close()
    return data

def save_jsonlines(data_id, data_iter):
    path = get_data_path(data_id)
    f = open(path, 'w')
    for data in data_iter:
        f.write(json.dumps(data) + '\n')
    f.close()

def load_jsonlines_iter(data_id):
    path = get_data_path(data_id)
    f = open(path, 'r')
    for line in f:
        data = json.loads(line.strip())
        yield data
    f.close()

def load_jsonlines(data_id):
    return list(load_jsonlines_iter(data_id))

init_data_root_path()
